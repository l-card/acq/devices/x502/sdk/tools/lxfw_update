#ifndef LFIRM_CHECK_H__
#define LFIRM_CHECK_H__

#include <stdint.h>

typedef enum {
    LXFW_ERR_INVALID_PARAM          = -60000,
    LXFW_ERR_FIRM_FILE_OPEN         = -60001,
    LXFW_ERR_SIGN_FILE_OPEN         = -60002,
    LXFW_ERR_FILE_READ              = -60003,
    LXFW_ERR_INVALID_BOOT_FLAG      = -60004,
    LXFW_ERR_DEVICE_CHECK           = -60005, /* прошивка не подходит указанному устройству */
    LXFW_ERR_WRITE_FILE             = -60006,
    LXFW_ERR_DEVICE_OPEN            = -60007,
    LXFW_ERR_INVALID_INTERFACE      = -60008,
    LXFW_ERR_MEMORY_ALLOC           = -60009,
    LXFW_ERR_INVALID_SIGNATURE      = -60010,
    LXFW_ERR_FIRM_PARSE             = -60011,
    LXFW_ERR_FIRM_PARSE_ROOT_ELEM   = -60012,
    LXFW_ERR_FIRM_PARSE_NO_DATA     = -60013,
    LXFW_ERR_FIRM_PARSE_NO_DEVS     = -60014,
    LXFW_ERR_FIRM_OLD_VERSION       = -60015,
    LXFW_ERR_UNSUPPORTED_DEVICE     = -60016, /* Данное устройство не поддерживается */
    LXFW_ERR_NO_PROPERTY            = -60017, /* Не найденно нужное свойство в файле прошивки */
    LXFW_ERR_INVALID_PROPERTY_VALUE = -60018, /* Неверное значение свойства */
    LXFW_ERR_NO_DEVICE_FOUND        = -60019, /* Не удалось найти ни одного устройства */
    LXFW_ERR_GET_DEVLIST            = -60020
} t_lxfw_errs;



typedef struct st_lxfw_info t_lxfw_info;
typedef t_lxfw_info* t_lxfw_info_hnd;


t_lxfw_errs lxfw_check_sign(const char *filename);
t_lxfw_errs lxfw_init(const char *filename, t_lxfw_info_hnd *hinfo);
void lxfw_clean(t_lxfw_info_hnd hinfo);
void lxfw_cleanup(void);


const char *lxfw_get_property(t_lxfw_info_hnd hinfo, const char *name);
t_lxfw_errs lxfw_get_wr_sign(t_lxfw_info_hnd hinfo, uint32_t *sign);
t_lxfw_errs lxfw_get_crc32(t_lxfw_info_hnd hinfo, uint32_t *sign);
t_lxfw_errs lxfw_get_size(t_lxfw_info_hnd hinfo, unsigned *size);
const char *lxfw_get_version(t_lxfw_info_hnd hinfo);
const char *lxfw_get_devname(t_lxfw_info_hnd hinfo);

t_lxfw_errs firm_check_device(t_lxfw_info_hnd hinfo, const char *devname,
                              const char *chip, const char *rev);
int lxfw_get_data(t_lxfw_info_hnd hinfo, unsigned char *buf, size_t pos, size_t size);


const char *lxfw_get_err_str(t_lxfw_errs err_code);



#endif
