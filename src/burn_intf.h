#ifndef BURN_INTF_H__
#define BURN_INTF_H__
#include "l502api.h"
#include "firm_check.h"
#include "ltimer.h"

#define LXFW_MAX_FILENAMES  8

typedef struct st_burn_intf t_burn_intf;


typedef enum {
    L502BURN_OP_WRITE,
    L502BURN_OP_READ
} t_l502burn_op;

typedef struct {
    const char *serial;
    const char *devname;
    int port;
    t_l502burn_op op;

    int filenames_cnt;
    const char* filenames[LXFW_MAX_FILENAMES];
    t_lxfw_info_hnd firm_infos[LXFW_MAX_FILENAMES];

    t_lxfw_info_hnd cur_fw_info;

    int read_start;
    int read_last;
    int invers;
    int all;
    int ver_dont_check;
    int main_only;
    int reserv_only;
    t_lclock_ticks con_tout; /** таймаут, в течении которого идет попытка установить связь с устройством */
    const t_burn_intf* intf;

    int suc_updated_devcnt; /* количесто обновленных устройств */
} t_lxfw_burn_opt;

typedef int (*t_exec_cmd_cb)(t_lxfw_burn_opt* opts);

typedef int (*t_intf_open)(t_lxfw_burn_opt* opts);
typedef int (*t_intf_close)(void);
typedef int (*t_check_dev)(t_lxfw_info_hnd fw);
typedef int (*t_check_version)(t_lxfw_burn_opt *opts, const char* version);
typedef int (*t_intf_flash_set_prot)(uint8_t prot);
typedef int (*t_intf_flash_erase_64k)(uint32_t addr);
typedef int (*t_intf_flash_write_start)(uint32_t addr, const uint8_t* data, uint32_t size);
typedef int (*t_intf_flash_write_next)( const uint8_t* data, uint32_t size);
typedef int (*t_intf_flash_write_stop)(void);
typedef int (*t_intf_flash_read)(uint32_t addr, uint8_t *data, uint32_t size);
typedef int (*t_intf_exec_for_all)(t_lxfw_burn_opt* opts, t_exec_cmd_cb cb);
typedef int (*t_intf_reload_info)(void);
typedef const char* (*t_intf_get_err_str)(int err_code);



struct st_burn_intf {
    const char* name;
    t_intf_open open;
    t_intf_close close;
    t_check_dev check_dev;
    t_check_version check_ver;
    t_intf_flash_set_prot flash_set_prot;
    t_intf_flash_erase_64k flash_erase_64k;
    t_intf_flash_write_start flash_write_start;
    t_intf_flash_write_next flash_write_next;
    t_intf_flash_write_stop flash_write_stop;
    t_intf_flash_read flash_read;
    t_intf_reload_info reload_info;
    t_intf_exec_for_all exec_for_all;
    t_intf_get_err_str  get_err_str;
};



typedef int (*t_write_firm)(t_lxfw_burn_opt* opts);

typedef struct st_burn_dev {
    const char* name;
    t_write_firm* write_firm;
} t_burn_dev;



#endif
