#include "portaddr.h" 
#include <stdio.h> 



HKEY hPnp,hId; 

DWORD reg_sz=REG_SZ, reg_resource_list=REG_RESOURCE_LIST; 

char IdKey[128], DataKey[128], ResData[512], pName[8];


//-------------------------------------------------------------------------------- 

int GetPortAddresses(PORTS_LIST* PortData) 
{ 
    DWORD ResDataLen, pNameLen, IdKeyLen;
    OSVERSIONINFO ovi;
    int NT;

    ResDataLen=sizeof(ResData);
    pNameLen=sizeof(pName);
    IdKeyLen=sizeof(IdKey);

    ovi.dwOSVersionInfoSize=sizeof(ovi); 
    GetVersionEx(&ovi); 
    if(ovi.dwMajorVersion==4 && ovi.dwMinorVersion==0) 
        NT=1; 
    else
        NT=0; 

   if(RegOpenKeyEx(HKEY_LOCAL_MACHINE 
            ,"SYSTEM\CurrentControlSet\Enum\ACPI\PNP0400" 
            ,0 
            ,KEY_ENUMERATE_SUB_KEYS 
            ,&hPnp) 
            !=ERROR_SUCCESS) 
        return -1; 

   for(PortData->Count=0;;PortData->Count++) 
   { 
          if(RegEnumKeyEx(hPnp 
             ,PortData->Count 
             ,IdKey 
             ,&IdKeyLen 
             ,NULL 
             ,NULL 
             ,NULL 
             ,NULL) 
             !=ERROR_SUCCESS) 
                 { 
                  RegCloseKey(hPnp); 
              return 0; 
                 } 

          sprintf(DataKey,"%sDevice Parameters",IdKey); 
          if(RegOpenKeyEx(hPnp 
             ,DataKey 
             ,NULL 
             ,KEY_READ 
             ,&hId) 
             !=ERROR_SUCCESS) 
             { 
                  RegCloseKey(hPnp); 
              return -1; 
                 } 

          if(RegQueryValueEx(hId 
             ,"PortName" 
             ,NULL 
             ,&reg_sz 
             ,(BYTE*)&pName 
             ,&pNameLen) 
             !=ERROR_SUCCESS) 
             { 
                  RegCloseKey(hPnp); 
                  RegCloseKey(hId); 
              return -1; 
                 } 
          sprintf(PortData->pData[PortData->Count].Name,pName); 
    
          if(NT) 
            sprintf(DataKey,"%sLogConf",IdKey); 
          else 
            sprintf(DataKey,"%sControl",IdKey); 
          if(RegOpenKeyEx(hPnp 
             ,DataKey 
             ,NULL 
             ,KEY_READ 
             ,&hId) 
             !=ERROR_SUCCESS) 
             { 
                  RegCloseKey(hPnp); 
                  RegCloseKey(hId); 
              return -1; 
                 } 

          if(NT) 
            sprintf(DataKey,"BootConfig"); 
          else 
            sprintf(DataKey,"AllocConfig"); 
          if(RegQueryValueEx(hId 
             ,DataKey 
             ,NULL 
             ,&reg_resource_list 
             ,(BYTE*)&ResData 
             ,&ResDataLen) 
             !=ERROR_SUCCESS) 
             { 
                  RegCloseKey(hPnp); 
                  RegCloseKey(hId); 
              return -1; 
                 } 
             RegCloseKey(hId);
             PortData->pData[PortData->Count] 
                                .Address=((PCM_RESOURCE_LIST)ResData)->List[0] 
                                .PartialResourceList.PartialDescriptors[0] 
                                .u.Port.Start; 
         } 
   return 0; 
}