#include "byteblaster.h"
#include <Windows.h>
#include "Dlportio.h"


void byteblaster_init(int port)
{
    int rd=0;
    DlPortWritePortUchar(port, BB_BIT_TDI | BB_BIT_TMS | BB_BIT_TCK);
    DlPortWritePortUchar(port+2, BB_BIT_OE);
}


void byteblaster_close(int port)
{
    /* ��������� �������� ������� */
    DlPortWritePortUchar(port+2, 0);
}


int byteblaster_write(int port, int byte)
{
    int res = 0;
    DlPortWritePortUchar(port, byte);
    res = DlPortReadPortUchar(port+1);
    return ((res & BB_BIT_TDO) == 0);
}

int byteblaster_read_do(int port)
{
    int res = 0;
    res = DlPortReadPortUchar(port+1);
    return ((res & BB_BIT_TDO) == 0);
}
