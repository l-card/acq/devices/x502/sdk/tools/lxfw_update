#ifndef SST25_H__
#define SST25_H__


/** ���� ������ */
typedef enum
{
    SST25_ERR_INVALID_HANDLE       = -1,
    SST25_ERR_UNSUP_MF_ID          = -2,
    SST25_ERR_UNSUP_DEV_ID         = -3,
    SST25_ERR_INVALID_WRITE_STATUS = -4,
    SST25_ERR_WRITE_TOUT           = -5,
} t_sst25_errs;

/** ���� �������� ������� */
typedef enum
{
    SST25_STATUS_BUSY   = 0x01,
    SST25_STATUS_WEL    = 0x02,
    SST25_STATUS_BP0    = 0x04,
    SST25_STATUS_BP1    = 0x08,
    SST25_STATUS_BP2    = 0x10,
    SST25_STATUS_BP3    = 0x20,
    SST25_STATUS_AAI    = 0x40,
    SST25_STATUS_BPL    = 0x80
} t_sst25_status_bits;







typedef struct
{
    int lpt_io; /**< ����� � ������������ io ������� ����� lpt */
    int mf_id; /**< Manufacturer ID */
    int dev_id; /**< Device ID */
    int status; /**< ������ �� ���������� �������� */
} t_sst25;

int sst25_init(t_sst25* hnd, int lpt_io);
int sst25_read(t_sst25* hnd, int addr, char* buf, int size);
int sst25_write(t_sst25* hnd, int addr, const char* buf, int size);

int sst25_write_start(t_sst25* hnd, int addr, const char* buf, int size);
int sst25_write_next(t_sst25* hnd, const char* buf, int size);
int sst25_write_stop(t_sst25* hnd);

int sst25_set_protection(t_sst25* hnd, int protect_code);

int sst25_erase_chip(t_sst25* hnd);
int sst25_erase_4k(t_sst25* hnd, int addr);
int sst25_erase_32k(t_sst25* hnd, int addr);
int sst25_erase_64k(t_sst25* hnd, int addr);

const char* sst25_get_err_string(int err);

void sst25_close(t_sst25* hnd);





#endif