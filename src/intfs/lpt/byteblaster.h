#ifndef BYTEBLASTER_H__
#define BYTEBLASTER_H__

#define BB_BIT_TCK  0x01
#define BB_BIT_TMS  0x02
#define BB_BIT_TDI  0x40

#define BB_BIT_TDO  0x80

#define BB_BIT_OE   0x02


void byteblaster_init(int port);
void byteblaster_close(int port);

int byteblaster_write(int port, int byte);
int byteblaster_read_do(int port);




#endif