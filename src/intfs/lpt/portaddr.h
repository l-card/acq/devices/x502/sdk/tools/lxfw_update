// PortAddr.h 
// Some part of ntddk.h file and few my structures 
//------------------------------------------------------------------------------ 
#pragma once 
#include <windows.h> 
typedef enum _INTERFACE_TYPE 
{ 
    InterfaceTypeUndefined = -1, 
    Internal, 
    Isa, 
    Eisa, 
    MicroChannel, 
    TurboChannel, 
    PCIBus, 
    VMEBus, 
    NuBus, 
    PCMCIABus, 
    CBus, 
    MPIBus, 
    MPSABus, 
    ProcessorInternal, 
    InternalPowerBus, 
    PNPISABus, 
    PNPBus, 
    MaximumInterfaceType 
}INTERFACE_TYPE, *PINTERFACE_TYPE; 

typedef LARGE_INTEGER PHYSICAL_ADDRESS, *PPHYSICAL_ADDRESS; 

#pragma pack(4) 
typedef struct _CM_PARTIAL_RESOURCE_DESCRIPTOR 
{ 
    UCHAR Type; 
    UCHAR ShareDisposition; 
    USHORT Flags; 
    union 
   { 
        struct 
      { 
            PHYSICAL_ADDRESS Start; 
            ULONG Length; 
        } Generic; 

        struct 
      { 
            PHYSICAL_ADDRESS Start; 
            ULONG Length; 
        } Port; 

        struct 
      { 
            ULONG Level; 
            ULONG Vector; 
            ULONG Affinity; 
        } Interrupt; 

        struct 
      { 
            PHYSICAL_ADDRESS Start;    // 64 bit physical addresses. 
            ULONG Length; 
        } Memory; 

        struct 
      { 
            ULONG Channel; 
            ULONG Port; 
            ULONG Reserved1; 
        } Dma; 

        struct 
      { 
            ULONG Data[3]; 
        } DevicePrivate; 

        struct 
      { 
            ULONG Start; 
            ULONG Length; 
            ULONG Reserved; 
        } BusNumber; 

        struct 
      { 
            ULONG DataSize; 
            ULONG Reserved1; 
            ULONG Reserved2; 
        } DeviceSpecificData; 
    } u; 
} CM_PARTIAL_RESOURCE_DESCRIPTOR, *PCM_PARTIAL_RESOURCE_DESCRIPTOR; 
#pragma pack() 

typedef struct _CM_PARTIAL_RESOURCE_LIST 
{ 
    USHORT Version; 
    USHORT Revision; 
    ULONG Count; 
    CM_PARTIAL_RESOURCE_DESCRIPTOR PartialDescriptors[1]; 
} CM_PARTIAL_RESOURCE_LIST, *PCM_PARTIAL_RESOURCE_LIST; 

typedef struct _CM_FULL_RESOURCE_DESCRIPTOR 
{ 
    INTERFACE_TYPE InterfaceType; 
    ULONG BusNumber; 
    CM_PARTIAL_RESOURCE_LIST PartialResourceList; 
} CM_FULL_RESOURCE_DESCRIPTOR, *PCM_FULL_RESOURCE_DESCRIPTOR; 

typedef struct _CM_RESOURCE_LIST 
{ 
    ULONG Count; 
    CM_FULL_RESOURCE_DESCRIPTOR List[1]; 
} CM_RESOURCE_LIST, *PCM_RESOURCE_LIST; 
//------------------------------------------------------------------------------ 

typedef struct _PORT_DATA 
{ 
   char Name[8]; 
   LARGE_INTEGER Address; 
}PORT_DATA; 

typedef struct _PORTS_LIST 
{ 
   int Count; 
   PORT_DATA pData[1]; 
}PORTS_LIST;

int GetPortAddresses(PORTS_LIST* list);