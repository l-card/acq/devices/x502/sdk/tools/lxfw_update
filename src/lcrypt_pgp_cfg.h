#ifndef LCRYPT_PGP_CFG_H
#define LCRYPT_PGP_CFG_H


/** Признак, что не используются не один вариант криптографического стека,
    соответственно MPI все заданного размера, и включены только функции разбора,
    без функций проверки подписи */
#define LCRYPT_CRYPT_ENGINE_NONE

/** Максимальный размер MPI-чисел, если используется статический размер */
#define LCRYPT_MPI_MAX_SIZE 512


#endif // LCRYPT_PGP_CFG_H
