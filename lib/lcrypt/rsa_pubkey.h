#ifndef __RSA_PUBKEY_H__ 
#define __RSA_PUBKEY_H__

#include "stdint.h"

#define RSA_N_SIZE (256)
#define RSA_E_SIZE (3)

extern const uint8_t rsa_n[RSA_N_SIZE];
extern const uint32_t rsa_e;

#endif
