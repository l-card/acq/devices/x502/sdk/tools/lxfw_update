/*
 * pgp.h
 *
 *  Created on: 12.03.2010
 *      Author: borisov
 */

#ifndef PGP_H_
#define PGP_H_

#include "stdint.h"

#define SIGN_MSG_SIZE      (280)
#define SIGN_SIZE          (256)


#define SHA256_ASN_SIZE (19)

extern const uint8_t SHA256_ASN[SHA256_ASN_SIZE];

#endif /* PGP_H_ */
