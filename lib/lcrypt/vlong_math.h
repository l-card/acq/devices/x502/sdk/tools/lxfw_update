/*
 * vlong_math.h
 *
 *  Created on: 12.03.2010
 *      Author: borisov
 */

#ifndef __VLONG_MATH_H__
#define __VLONG_MATH_H__

#include <stdint.h>

#define BASE 0x100 //основание для каждого элемента массива при представлении длинных чисел


typedef uint8_t long_t; //тип элемента массива при задании больших чисел
typedef int32_t ind_t;  //тип для задания размера массива и индексных переменных
typedef int32_t tmp_t;  //временный тип - должен вмещать как минимум BASE**2 + BASE (надо проверить, мб >)
							//и быть signed
typedef int32_t slong_t;  //тип простого числа для операций между простым числом и длинным


#ifdef __cplusplus
extern "C" {
#endif

ind_t vlong_add(const long_t *a, ind_t a_size, const long_t* b, ind_t b_size, long_t *c);
ind_t vlong_smul(const long_t *a, ind_t a_size, long_t b, long_t *c);
ind_t vlong_mul(const long_t *a, ind_t a_size, const long_t *b, ind_t b_size, long_t *c);
ind_t vlong_fast_pow(long_t *a, ind_t a_size, long_t *b, ind_t b_size, long_t *c);
ind_t vlong_sdiv(const long_t *a, ind_t a_size, slong_t b, long_t *q, slong_t* r1);
ind_t vlong_div(const long_t *a, ind_t a_size, long_t* b, ind_t b_size, long_t *q, ind_t *q_size, long_t *u, ind_t *pu_size);
ind_t vlong_fast_spow_mod(const long_t *a, ind_t a_size, uint32_t e, long_t *n, ind_t n_size, long_t *pow, long_t *t, long_t *buf);


#ifdef __cplusplus
}
#endif

#endif
