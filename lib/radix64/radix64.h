#ifndef __RADIX64_H___
#define __RADIX64_H___

#ifdef __cplusplus 
extern "C" {
#endif
int Radix64_Encode(const unsigned char* data, int size, char* msg);


int Radix64_Decode(const char* msg, int size, unsigned char* data);

#ifdef __cplusplus 
}
#endif


#endif
